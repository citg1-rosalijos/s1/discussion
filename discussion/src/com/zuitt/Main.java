// directory that contains files that is useful to build an application.
// reverse domain name notation
package com.zuitt;

import java.util.Locale;

// "Main" class is the entry point of a java program and is responsible for execute our code.
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World");

        // variables
        int myNum;
        int myNum2 = 21;

        System.out.print("Results of variable declaration and initialization: ");
        System.out.println(myNum2);

        // constants
        final int PRINCIPAL = 1000;

        // two data types
        // primitive (single values) and non-primitive (object methods)

        char letter = 'A';
        boolean isMarried = false;

        // -128 to 127
        byte students = 127;

        // -32768 to 32767
        short seats = 32767;

        // -2147483648 to 2147483647
        // underscores maybe placed in between numbers for readability
        int localPopulation = 2_147_483_647;

        long worldPopulation = 7_862_081_145L;

        // floats and double
        // depends on preciseness
        float price = 12.99F;
        double temperature = 15869.8623941;

        System.out.print("Results of getClass method: ");
        System.out.println(((Object) temperature).getClass());

        String name = "John Doe";
        System.out.print("Name: ");
        System.out.println(name);
        System.out.print("Lowercase: ");
        System.out.println(name.toLowerCase());
        System.out.print("Uppercase: ");
        System.out.println(name.toUpperCase());

        // implicit casting
        int num1 = 5;
        double num2 = 2.7;
        double total = num1 + num2;
        int anotherTotal = num1 + (int) num2;
        System.out.println("Results from implicit casting: " + total);
        System.out.println("Results from explicit casting: " + anotherTotal);
    }
}
